from django.urls import include, path
from .views import index, arawinda, savename, readperson

urlpatterns = [
    path('iis', index, name='iis'),
    path('arawinda', arawinda, name='arawinda'),
    path('savename', savename),
    path('readperson', readperson)
]
