from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Person, Post

response = {}

def index(request):
    response = {'name' : 'Iis Afriyanti',
    			'input_form' : Input_Form}
    return render(request,'index.html',response)

def arawinda(request):
    response = {'name' : 'Arawinda Afriyanti'}
    return render(request,'index.html',response)

def savename(request):
	form = Input_Form(request.GET or None)
	if (form.is_valid and request.method == 'GET'):
		form.save()
		return HttpResponseRedirect('/iis')
	else:
		return HttpResponseRedirect('/iis')

def readperson(request):
	persons = Person.objects.all()
	response['name'] = 'Iis Afriyanti'
	response['persons'] = persons

	html = 'index.html'
	return render(request, html, response)